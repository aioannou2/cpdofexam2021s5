package com.agiletestingalliance;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class UnusedTestClassTest {

    @Test
    public void testDesc() throws Exception {
        assertEquals("str", new UnusedTestClass("test").gstr(), "test");
    }
}
