package com.agiletestingalliance;
import static org.junit.Assert.*;
import org.junit.Test;

public class MinMaxTest {

	@Test
	public void testFindMax() {

		int max = new MinMax().findMax(5, 6);
		assertEquals("max", 6,max);
	}

}
